const { fighter } = require('../models/fighter');
const { compareObjects } = require("./user.validation.middleware");

const checkFieldsIsValid = (rest, body) => {
    for (let key in body) {
        if(!rest.hasOwnProperty(key)) {
            return {
                error: true,
                message: "Invalid  properties",
                status: 400
            }
        }
        if (typeof rest[key] !== typeof body[key]) {
            return {
                error: true,
                message: "Invalid types of params",
                status: 400
            }
        }
    }
    if (body.power <= 0 || body.power >= 100) {
        return {
            error: true,
            message: "Power not valid",
            status: 400
        };
    }
    if (body.defense < 1 || body.defense >= 10) {
        return {
            error: true,
            message: "Defense not valid",
            status: 400
        };
    }
    return true;
}

const validateCreateFighter = (base, body) => {
    const { id, ...rest } = base;
    return new Promise((resolve, reject) => {
       const validation = checkFieldsIsValid(rest, body);
       if (validation.error) {
        reject(validation);
       }
       resolve(true);
    }); 
}

const createFighterValid = async (req, res, next) => {
    const credentials = req.body;
    const {id, health, ...requiered} = fighter;
    const { health: exclude, ...requieredParams } = credentials;
    try {
        const isRequired = compareObjects(requiered, requieredParams);
        if (!isRequired) {
            throw ({
                error: true,
                status: 400,
                message: "Pass all required fields"
            })
        }
        await validateCreateFighter(fighter, credentials);
        if (!credentials.health) {
            req.body.health = 100;
        }
        next();
    } catch (error) {
        return res.status(error.status)
            .json({
                error: error.error,
                message: error.message
            });
    }
}

const updateFighterValid = async (req, res, next) => {
    const credentials = req.body;
    try {
        await validateCreateFighter(fighter, credentials);
        next();
    } catch (error) {
        return res.status(error.status)
            .json({
                error: error.error,
                message: error.message
            });
    }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;