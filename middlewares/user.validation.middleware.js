const { user } = require('../models/user');

function compareObjects(rest, body) {
    const baseKeys = Object.keys(rest).sort();
    const bodyKeys = Object.keys(body).sort();
    return JSON.stringify(baseKeys) === JSON.stringify(bodyKeys)
}

function validateSpecailCredentials(body) {
    const emailRegExp = new RegExp(/^[\w.+\-]+@gmail\.com$/);
    const phoneRegExp = new RegExp(/^\+380[0-9]{9}$/);
    const passwordRegExp = new RegExp(/^(?=.{3,})/);
    const { email, password, phoneNumber } = body;
    if (email === undefined && password === undefined && phoneNumber === undefined) {
        return true;
    }
    if (email && !emailRegExp.test(email)) {
        return {
            error: true,
            message: "Enter valid email. Only gmail."
        }
    }
    if (password && !passwordRegExp.test(password)) {
        return {
            error: true,
            message: "Enter valid password. Min 3 symbols"
        }
    }
    if (phoneNumber && !phoneRegExp.test(phoneNumber)) {
        return {
            error: true,
            message: "Enter valid phone number. +380xxxxxxxxx"
        }
    }
    return true;
}

function validateCreate(base, body) {
    const { id, ...rest } = base;
    return new Promise((resolve, rejected) => {
        const isValidKeys = compareObjects(rest, body);
        if (!isValidKeys) {
            rejected({
                error: true,
                message: "Not valid credentials"
            });
        }
        const checkSpecialRules = validateSpecailCredentials(body);
        if (checkSpecialRules.error) {
            rejected({
                error: checkSpecialRules.error,
                message: checkSpecialRules.message
            });
        }
        resolve(true);
    });
}

const createUserValid = async (req, res, next) => {
    const credentials = req.body;
    try {
        await validateCreate(user, credentials);
        next();
    } catch (error) {
        return res.status(400).json({
            error: error.error,
            message: error.message
        });
    }
}

function validateUpdate(base, body) {
    const { id, ...rest } = base;
    const copy = {
        ...rest,
        ...body
    };
    return new Promise((resolve, rejected) => {
        const isValidKeys = compareObjects(rest, copy);
        if (!isValidKeys) {
            rejected({
                error: true,
                message: "Not valid credentials"
            });
        }
        const checkSpecialRules = validateSpecailCredentials(copy);
        if (checkSpecialRules.error) {
            rejected({
                error: checkSpecialRules.error,
                message: checkSpecialRules.message
            });
        }
        resolve(true);
    });
}

const updateUserValid = async (req, res, next) => {
    const credentials = req.body;
    try {
        await validateUpdate(user, credentials);
        next();
    } catch (error) {
        return res.status(400).json({
            error: error.error,
            message: error.message
        });
    }
}

exports.compareObjects = compareObjects;

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;