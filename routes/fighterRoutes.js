const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get("/", (req, res, next) => {
  try {
    const fighters = FighterService.getAll();
    if (fighters.length === 0) {
      throw ({
        error: true,
        status: 404,
        message: "Not found"
      })
    }
    res.data = fighters;
  } catch (error) {
    res.err = error;
  } finally {
    next();
  }
}, responseMiddleware);

router.get("/:id", (req, res, next) => {
  const { id } = req.params;
  try {
    const fighter = FighterService.search({ id });
    if (!fighter) {
      throw ({
        error: true,
        status: 404,
        message: "Fighter not found"
      })
    }
    res.data = fighter;
  } catch (error) {
    error.error ? res.err = error : res.err = {
      error: true,
      status: 500,
      message: "Error. Can't get information about fighter" 
    }
  } finally {
    next();
  }
}, responseMiddleware);

router.post("/", createFighterValid, (req, res, next) => {
  const fighter = req.body;
  try {
    const createdFighter = FighterService.createFighter(fighter);
    if (createdFighter.error) {
      throw (createdFighter);
    }
    res.data = createdFighter;
  } catch (error) {
    res.err = error;
  } finally {
    next();
  }
}, responseMiddleware);

router.put("/:id", updateFighterValid, (req, res, next) => {
  const { id } = req.params;
  try {
    const updated = FighterService.updateFighter(id, req.body);
    if (updated.error) {
      throw (updated);
    }
    res.data = updated;
  } catch (error) {
    res.err = error;
  } finally {
    next();
  }
}, responseMiddleware);

router.delete("/:id", (req, res, next) => {
  const { id } = req.params;
  try {
    const result = FighterService.deleteFighter(id);
    if (result.error) {
      throw (result);
    }
    res.data = "Fighter deleted successfully";
  } catch (error) {
    res.err = error;
  } finally {
    next();
  }
}, responseMiddleware);

module.exports = router;