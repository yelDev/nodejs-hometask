const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    const { email, password } = req.body;
    try {
        const getUserCredentials = AuthService.login({ email });
        if (!getUserCredentials) {
            throw new Error("User not found") 
        };
        const psw = getUserCredentials.password;
        psw === password ? res.data = { status: 200, message: "Log-in"} : res.err = {
            error: true,
            message: "Incorrect password",
            status: 400
        };
    } catch (err) {
        let message = "Error";
        if (error.name === "Error") {
          message = "Not valid credentials";
        }
        res.err = {
          error: true,
          message,
          status: 400
        };
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;