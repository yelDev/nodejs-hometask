const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// create user
router.post("/", createUserValid, (req, res, next) => {
  const credentials = req.body;
  try {
    const user = UserService.create(credentials);
    if (user.error) {
      throw (user)
    }
    res.data = user;
  } catch (error) {
    res.err = error;
  } finally {
    next();
  }
}, responseMiddleware);

// get users
router.get("/", (req, res, next) => {
  try {
    const users = UserService.getUsers();
    if (users.length === 0) {
      throw ({
        error: true,
        message: "Users not found",
        status: 404
      })
    }
    res.data = users;
  } catch (error) {
    res.err = error;
  } finally {
    next();
  }
}, responseMiddleware);

// get user
router.get("/:id", (req, res, next) => {
  const { id } = req.params;
  try {
    const user = UserService.search({ id });
    if (!user) {
      throw ({
        status: 404,
        message: "User not found",
        error: true
      })
    }
    res.data = user;
  } catch (error) {
    res.err = error;
  } finally {
    next();
  }
}, responseMiddleware);

// delete user
router.delete("/:id", (req, res, next) => {
  const { id } = req.params;
  try {
    const user = UserService.deleteUser(id);
    if (user.error) {
      throw (user);
    }
    res.data = "Account deleted successfully";
  } catch (error) {
    res.err = error;
  } finally {
    next();
  }
}, responseMiddleware);

// update user
router.put("/:id", updateUserValid, (req, res, next) => {
  const { id } = req.params;
  try {
    const updated = UserService.updateUser(id, req.body);
    if (updated.error) {
      throw (updated)
    }
    res.data = updated;
  } catch (error) {
    res.err = error;
  } finally {
    next();
  }
}, responseMiddleware);

module.exports = router;