const { UserRepository } = require('../repositories/userRepository');

class UserService {
    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    create(credentials) {
        const checkUser = this.search({ email: credentials.email });
        if (checkUser) {
            return {
                error: true,
                message: "Email already exist",
                status: 400
            }
        }
        const newUser = UserRepository.create(credentials);
        return newUser;
    }

    getUsers() {
        const data = UserRepository.getAll();
        return data;
    }

    updateUser(id, data) {
        if (!this.search({ id })) {
            return {
                error: true,
                message: "User not found",
                status: 404
            };
        }
        return UserRepository.update(id, data);
    }

    deleteUser(id) {
        if (!this.search({ id })) {
            return {
                error: true,
                message: "User not found",
                status: 404
            };
        }
        return UserRepository.delete(id);
    }
}

module.exports = new UserService();