const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    createFighter(data) {
        const isUniqName = this.search({ name: data.name });
        if (isUniqName) {
            return {
                error: true,
                message: "Please, enter unique name",
                status: 400
            }
        }
        const result = FighterRepository.create(data);
        return result;
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    getAll() {
        return FighterRepository.getAll();
    }

    deleteFighter(id) {
        if (!this.search({ id })) {
            return {
                error: true,
                status: 404,
                message: "Fighter not found"
            }
        }
        return FighterRepository.delete(id)
    }

    updateFighter(id, data) {
        const checkUser = this.search({ id });
        if (!checkUser) {
            return {
                error: true,
                message: "Fighter is not exist",
                status: 400
            }
        }
        return FighterRepository.update(id, data);
    }
}

module.exports = new FighterService();